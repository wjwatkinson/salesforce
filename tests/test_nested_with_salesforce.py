from salesforce import Connector
from typing import List
import os
import uuid

def test_nested_insert(conn: Connector) -> dict:
    account = {'name': 'nested'}
    attr_acc = conn.add_attributes(account, 'account', 'acc')
    contacts = [{'lastname': 'nestle'}, {'lastname': 'nestea'}]
    attr_conts = [conn.add_attributes(c, 'contact', f'cont{str(i)}') for i, c in enumerate(contacts)]
    cont_nest = {'contacts': attr_conts}
    data = conn.build_nested(attr_acc, cont_nest)
    return conn.nested_insert([data], 'account')


conn = Connector(os.getenv("UN"), os.getenv("PW"), os.getenv("AT"))

response = test_nested_insert(conn)

ids = [r['id'] for r in response['results']]

conn.bulk_delete(ids)