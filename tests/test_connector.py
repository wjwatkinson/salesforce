from salesforce.connector import Connector
from unittest.mock import patch
import pytest


class TestUnitConnector:
    class FakeConnector(Connector):
        def __init__(self, *args, **kwargs):
            self.base_url = "base_url/"
            pass

    fc = FakeConnector()

    def test_build_nested(self):
        a = {"name": "test"}
        sub = {
            "Contacts": [{"email": "bill@gmail.com"}, {"email": "james@gmail.com"}],
            "Orders": [{"total": 1234}],
        }
        expected = {
            "name": "test",
            "Contacts": {
                "records": [
                    {"email": "bill@gmail.com"},
                    {"email": "james@gmail.com"},
                ]
            },
            "Orders": {"records": [{"total": 1234}]},
        }
        assert self.fc.build_nested(a, sub) == expected

    def test__chunk(self):
        data = [{"test": "test"} for _ in range(1000)]
        chunked = self.fc._chunk(data, 200)
        assert len(chunked) == 5
        assert len(chunked[1]) == 200

    def test_add_attributes_w_ref(self):
        data = {"data": "data"}
        expected = {
            "data": "data",
            "attributes": {"type": "orderItem", "referenceId": "refId"},
        }
        assert self.fc.add_attributes(data, "orderItem", "refId") == expected

    def test_add_attributes_no_ref(self):
        data = {"data": "data"}
        expected = {"data": "data", "attributes": {"type": "orderItem"}}
        assert self.fc.add_attributes(data, "orderItem") == expected

    def test_add_attributes_dict(self):
        data = {"data": "data"}
        kwargs = {"kwarg": "kwarg"}
        expected = {
            "data": "data",
            "attributes": {
                "type": "orderItem",
                "referenceId": "refId",
                "kwarg": "kwarg",
            },
        }
        assert self.fc.add_attributes(data, "orderItem", "refId", kwargs) == expected

    def test__sobject_url(self):
        obj_name = "test"
        assert self.fc._sobject_url(obj_name) == "base_url/sobjects/test/"

    def test__id_url(self):
        obj_name = "test"
        rec_id = "AAA"
        assert self.fc._id_url(rec_id, obj_name) == "base_url/sobjects/test/AAA"
