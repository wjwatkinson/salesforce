# pipenv run python -m tests.test_individual_with_salesforce
from salesforce import Connector
import os
import uuid


def create(c: Connector) -> str:
    data = {"name": uuid.uuid4().hex}
    account_id = c.create("Account", data)
    assert account_id
    return account_id


def query(c: Connector):
    assert c.query("Account", ["Id"], "Limit 5")


def update(c: Connector, account_id: str):
    data = {"name": "test"}
    c.update(account_id, "Account", data)
    cont = c.query("Account", ["name"], "WHERE id = '%s'", [account_id])
    assert cont
    assert cont[0]["Name"] == "test"


def delete(c: Connector, account_id: str):
    c.delete(account_id, "Account")
    cont = c.query("Account", ["name"], "WHERE id = '%s'", [account_id])
    assert not cont


c = Connector(os.getenv("UN"), os.getenv("PW"), os.getenv("AT"))

account_id = create(c)
query(c)
update(c, account_id)
delete(c, account_id)
