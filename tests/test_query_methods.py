from salesforce import query_methods
from unittest.mock import patch
import pytest


class TestQueryMethods:
    def test__find_val_pos(self):
        criteria = "where id = '%s' and name in %s"
        assert query_methods._find_val_pos(criteria, 1) == 28

    def test__flatten_values(self):
        values = ["a", "b", ["c", "d", "e"], "f", 2]
        expected = ["a", "b", "c", "d", "e", "f", 2]
        assert query_methods._flatten_values(values) == expected

    def test__replace_criteria_str(self):
        criteria = "where id = '%s' and name in %s and company != '%s'"
        values = ["c", "d", "d"]
        expected = "where id = '%s' and name in ('%s','%s','%s') and company != '%s'"
        assert query_methods._replace_criteria(criteria, 1, values) == expected

    def test__replace_criteria_other(self):
        criteria = "where id = '%s' and name in %s and company != '%s'"
        values = [1, 2, 3]
        expected = "where id = '%s' and name in (%s,%s,%s) and company != '%s'"
        assert query_methods._replace_criteria(criteria, 1, values) == expected

    def test__in_criteria(self):
        criteria = "where id = '%s' and name in %s and company != '%s'"
        values = ["fake-id", ["name", "test", "?"], "company"]
        expected = "where id = 'fake-id' and name in ('name','test','?') and company != 'company'"
        assert query_methods.in_criteria(criteria, values) == expected
