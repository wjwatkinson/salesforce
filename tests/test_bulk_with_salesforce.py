# pipenv run python -m tests.test_individual_with_salesforce
from salesforce import Connector
from typing import List
import os
import uuid

def account_data(qty: int) -> List[dict]:
    return [{"Name": uuid.uuid4().hex} for __ in range(qty)]


def bulk_create(c: Connector) -> List[dict]:
    data = account_data(400)
    return c.bulk_create(data, object_name="Account")


def _update_one_error_data(ids: List[str]) -> List[dict]:
    update_ids = ids[:2]
    up_data = [{'id': i, 'name': 'partial'} for i in update_ids]
    up_data.append({'id': 'failure', 'name': 'fail'})
    return up_data


def bulk_update_all_or_none_error(c: Connector, ids: List[dict]) -> List[dict]:
    c.bulk_update(_update_one_error_data(ids), object_name="Account")


def bulk_update_partial_error(c: Connector, ids: List[str]):
    c.bulk_update(_update_one_error_data(ids), all_or_none=False, object_name="Account")


def bulk_update(c: Connector, ids: List[str]):
    data = account_data(400)
    up_data = [{**data[i], **{'id': record_id}} for i, record_id in enumerate(ids)]
    c.bulk_update(up_data, object_name="Account")


def bulk_delete(c: Connector, ids: List[str]) -> List[dict]:
    c.bulk_delete(ids)

c = Connector(os.getenv("UN"), os.getenv("PW"), os.getenv("AT"))

acc_data = bulk_create(c)

ids = [r['id'] for r in acc_data]

bulk_update(c, ids)

bulk_update_partial_error(c, ids)

bulk_delete(c, ids)